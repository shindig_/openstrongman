from dataclasses import dataclass
from src.enums import Gender, WeightClass, TOMN
from typing import Dict, Optional


@dataclass
class Competition:
    name: str
    img_path: str
    ironpodium_link: str
    month: str
    day: int
    town: str
    state: str

    def __post_init__(self):
        self._id: str = f"{self.name}_{self.month}{self.day}"


@dataclass
class CompetitorClass:
    tomn: TOMN
    gender: Gender
    weight_class: WeightClass
    sub_weight: Optional[int] = None

    def __str__(self) -> str:
        if self.sub_weight:
            return "{} {} {}, -{}".format(
                self.tomn.value,
                self.gender.value,
                self.weight_class.value,
                self.sub_weight,
            )
        else:
            return "{} {} {}".format(
                self.tomn.value,
                self.gender.value,
                self.weight_class.value,
            )


@dataclass
class Strongman:
    first_name: str
    last_name: str
    weight_class: CompetitorClass
    place: Optional[int] = None
    event_dict: Optional[Dict] = None
