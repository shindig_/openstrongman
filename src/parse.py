from lxml import html
from typing import List, Tuple
from nameparser import HumanName
from src.enums import Gender, WeightClass, TOMN
from src.objects import Competition, CompetitorClass, Strongman


def get_upcoming_competitions(source: str) -> List[Competition]:
    tree = html.fromstring(source)
    comps = tree.xpath("//div[@class='card m-3 shadow-md deck-card']")
    for comp_card in comps[:10]:
        comp_title = comp_card.xpath(".//h5")[0].text_content()
        comp_link = comp_card.xpath("./a")[0].get("href")
        comp_pic = comp_card.xpath("./a/img")[0].get("src")
        comp_month = comp_card.xpath("./div/span/span[1]")[0].text_content()
        comp_day = comp_card.xpath("./div/span/span[2]")[0].text_content()
        comp_town, comp_state = (
            comp_card.xpath(".//p[@class='mb-1']")[0].text_content().split(",")
        )
        comp = Competition(
            comp_title,
            comp_pic,
            comp_link,
            comp_month,
            comp_day,
            comp_town,
            comp_state,
        )
        print(comp)
    pass


def parse_weight_class(wl: str) -> CompetitorClass:
    def parse_gwc(gwc: str) -> Tuple[Gender, WeightClass]:
        gender_dict = {
            "w": Gender.WOMEN,
            "m": Gender.MEN,
        }
        gender = gender_dict[gwc[0]]

        weight_dict = {
            "lw": WeightClass.LIGHT_WEIGHT,
            "mw": WeightClass.MIDDLE_WEIGHT,
            "hw": WeightClass.HEAVY_WEIGHT,
            "shw": WeightClass.SHW,
            "s": WeightClass.SINGLE_CLASS,
        }
        weight = weight_dict[gwc[1:]]
        return gender, weight

    components = wl.split("_")
    lbs = None
    if len(components) == 4:
        on, gwc, lbs, _ = components
    elif len(components) == 3:
        on, gwc, lbs = components
    elif len(components) == 2:
        on, gwc = components

    tomn = TOMN(on.capitalize())
    gender, weight_class = parse_gwc(gwc)
    if lbs:
        try:
            assert int(lbs) < 400
            return CompetitorClass(tomn, gender, weight_class, int(lbs))
        except (ValueError, AssertionError):
            return CompetitorClass(tomn, gender, weight_class)
    else:
        return CompetitorClass(tomn, gender, weight_class)


def get_events(content: str) -> List[str]:
    tree = html.fromstring(content)
    header, _ = tree.xpath("//thead/tr")

    events = []
    for child in header.getchildren():
        t = child.text
        if t and t != "Name":
            events.append(t.strip())

    return events


def strongman_from_html(top_element: html.HtmlElement) -> Strongman:
    """
    comp_elems = tree.xpath("//tr[@data-wc]")
    len(comp_elems[0].xpath(".//td"))
    """
    full_name = HumanName(
        top_element.xpath(".//td[@class='sticky-col first-col name']")[0]
        .text.strip()
        .title()
    )
    weight_class = parse_weight_class(top_element.values()[0])
    print(f"{full_name.first} {full_name.last} -> {weight_class}")


if __name__ == "__main__":
    from src.crawl import crawl, get_webdriver

    driver = get_webdriver()
    source = crawl("http://www.ironpodium.com/browse/events", driver, "source")
    get_upcoming_competitions(source)
