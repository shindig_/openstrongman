from enum import Enum


class WeightClass(Enum):
    SINGLE_CLASS = "(Single Class)"
    LIGHT_WEIGHT = "Lightweight"
    MIDDLE_WEIGHT = "Middleweight"
    HEAVY_WEIGHT = "Heavyweight"
    SHW = "Super Heavyweight"


class Gender(Enum):
    MEN = "Men"
    WOMEN = "Women"


class TOMN(Enum):
    TEEN = "Teen"
    OPEN = "Open"
    MASTER = "Masters"
    NOVICE = "Novice"
