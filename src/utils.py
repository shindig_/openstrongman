import logging
from datetime import date


def get_date(month: str, day: str, today: date) -> date:
    if month < today.month:
        return date(today.year + 1, month, day)
    else:
        return date(today.year, month, day)


def get_logger(name: str, level: int = 20) -> logging.Logger:
    FORMAT = "[%(asctime)s] - [%(levelname)s] - [%(funcName)s] - %(message)s"
    logging.basicConfig(level=level, format=FORMAT)
    return logging.getLogger(name)
