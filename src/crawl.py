from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

from typing import Union
from src.utils import get_logger

crawl_logger = get_logger("crawl")


def get_webdriver() -> None:
    options = Options()
    options.add_argument("--no-sandbox")
    options.add_argument("--headless")
    options.add_argument("--disable-dev-shm-usage")

    return webdriver.Chrome(
        service=Service(executable_path=ChromeDriverManager().install()),
        options=options,
    )


def crawl(
    url: str, driver: WebDriver, return_type: str = "driver"
) -> Union[str, WebDriver]:
    crawl_logger.info(f"Crawling {url}...")
    driver.get(url)
    if return_type == "driver":
        return driver
    elif return_type == "source":
        return driver.page_source
    else:
        raise KeyError(
            f"{return_type} is not valid, must be either 'driver' or 'source'"
        )


if __name__ == "__main__":
    driver = get_webdriver()
    base_url = "http://www.ironpodium.com/browse/events"
    sample_url = (
        "https://ironpodium.com/events/live/uss-maryland-strongest-2022"
    )
    # source, response = crawl(base_url, driver)
