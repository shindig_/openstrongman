from src.crawl import crawl, get_webdriver
from pytest import raises


def test_crawl():
    driver = get_webdriver()
    beer_muscles = "https://ironpodium.com/events/live/beer-muscles-2022"
    maryland = "https://ironpodium.com/events/live/uss-maryland-strongest-2022"
    bm = crawl(beer_muscles, driver)
    mary = crawl(maryland, driver)
    print(bm.title)
    print(mary.title)

    bm_source = crawl(beer_muscles, driver, "source")
    assert "html" in bm_source

    with raises(KeyError) as _:
        bm_source = crawl(beer_muscles, driver, "content")
