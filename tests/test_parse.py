from src.enums import TOMN, Gender, WeightClass
from src.objects import CompetitorClass
from src.parse import get_events, parse_weight_class

with open("./tests/assets/beer-muscles.txt", "r") as F:
    beer_muscles = F.read()

with open("./tests/assets/maryland-strongest.txt", "r") as F:
    maryland_strongest = F.read()


def test_get_events():
    assert get_events(beer_muscles) == [
        "Viking Press",
        "Ukrainian Keg Deadlift (Max)",
        "Giant Cornhole (Sandbag Toss)",
        "Keg Press (reps)",
        "Sled Push + Arm-Over-Arm Pull",
    ]
    assert get_events(maryland_strongest) == [
        "Monster Dbell Press",
        "Deadlift & Grip Medley",
        "Yoke",
        "Farmers Walk",
        "Conans Wheel",
    ]


def test_parse_weight_class():
    nwlw_140 = parse_weight_class("novice_wlw_140_nas")
    omhw_245 = parse_weight_class("open_mhw_245")
    mmshw = parse_weight_class("masters_mshw")
    mmshw_nas = parse_weight_class("masters_mshw_nas")
    nms = parse_weight_class("novice_ms")

    assert nwlw_140 == CompetitorClass(
        TOMN.NOVICE, Gender.WOMEN, WeightClass.LIGHT_WEIGHT, 140
    )
    assert omhw_245 == CompetitorClass(
        TOMN.OPEN, Gender.MEN, WeightClass.HEAVY_WEIGHT, 245
    )
    assert mmshw == CompetitorClass(TOMN.MASTER, Gender.MEN, WeightClass.SHW)
    assert mmshw_nas == CompetitorClass(
        TOMN.MASTER, Gender.MEN, WeightClass.SHW
    )
    assert nms == CompetitorClass(
        TOMN.NOVICE, Gender.MEN, WeightClass.SINGLE_CLASS
    )
