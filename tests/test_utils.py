from src.utils import get_date
from datetime import date


def test_get_date():
    today = date(2022, 10, 14)
    assert get_date(11, 12, today) == date(2022, 11, 12)
    assert get_date(8, 13, today) == date(2023, 8, 13)
    assert get_date(10, 15, today) == date(2022, 10, 15)
